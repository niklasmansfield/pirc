import pirc
import interface
import curses
import sys

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "Niklas"
REALNAME = "Niklas the Great"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)

# Special NICK with special color
special_nick = 'DrDoom'

def main(stdscreen):

    ui = interface.TextUI(stdscreen)

    while True:
        msg = irc.read()
        if msg:
            message_start = msg.find(':', 1)
            optional_start = msg.find('!~', 1, message_start)
        
            if optional_start > 0:
                user = msg[1:optional_start]
            else:
                user = msg[1:message_start]
            
            if user in special_nick:
                ui.set_text_color(interface.MAGENTA)
            ui.output('[' + user + '] ' + msg[message_start:])
            if user in special_nick:
                ui.set_text_color(interface.WHITE)
        
        usr_msg = ui.input()
        if usr_msg:
            if usr_msg.startswith('/MSG '):
                priv_user = usr_msg[5:usr_msg.find(' ', 5)]
                irc.send('PRIVMSG ' + priv_user + ' :' + usr_msg[usr_msg.find(' ', 5) + 1:])
                ui.output('*' + NICK + '* --> *' + priv_user + '* ' + usr_msg[usr_msg.find(' ', 5) + 1:])
            elif usr_msg.startswith('/QUIT'):
                irc.send('QUIT')
                sys.exit()
            elif usr_msg.startswith('/NAMES'):
                irc.send('NAMES ' + CHAN)
            else:
                irc.send('PRIVMSG #patwic :' + usr_msg)
                ui.set_text_color(interface.YELLOW)
                ui.output(' ' + NICK + ' ' + usr_msg)
                ui.set_text_color(interface.WHITE)
            

curses.wrapper(main)
